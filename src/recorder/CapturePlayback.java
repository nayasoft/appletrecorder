/*
 * @(#)CapturePlayback.java	1.11	99/12/03
 *
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Sun.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */
package recorder;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Line2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Policy;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;

import netscape.javascript.JSObject;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;



/**
 * Capture/Playback sample. Record audio in different formats and then playback
 * the recorded audio. The captured audio can be saved either as a WAVE, AU or
 * AIFF. Or load an audio file for streaming playback.
 * 
 * @version @(#)CapturePlayback.java 1.11 99/12/03
 * @author Brian Lichtenwalter
 */
@SuppressWarnings("serial")
public class CapturePlayback extends JApplet implements ActionListener
{

	final int bufSize = 16384;

	FormatControls formatControls = new FormatControls();
	Capture capture = new Capture();
	Playback playback = new Playback();

	AudioInputStream audioInputStream;
	ByteArrayInputStream byteArrayInputStream;
	SamplingGraph samplingGraph;

	TimeOut timeOut = new TimeOut();

	JButton playB, captB, pausB, loadB;
	JButton auB, aiffB, waveB;
	JTextField textField;
	String urlString = "";
	String siteHost;
	String fileName = "untitled";
	String errStr;
	double duration, seconds;
	File file;
	@SuppressWarnings("unchecked")
	Vector lines = new Vector();
	public static String defaultPath = "E:/wavefiles/";
	public static String extention = ".wav";
	public final static String prod="http://musicnoteapp.com/mnApi/mn1.0/Upload/audio";
	public final static String test="http://192.168.1.65:8080/mnApi/mn1.0/Upload/audio";
	public final static String Noteprod="http://musicnoteapp.com/mnApi/mn1.0/Upload/noteAudioUpload";
	public final static String Notetest="http://192.168.1.65:8080/mnApi/mn1.0/Upload/noteAudioUpload";
	
	public final static String asia="http://asia-lb-1085053260.ap-southeast-1.elb.amazonaws.com/mnApi/mn1.0/Upload/audio";
	public final static String Noteasia="http://asia-lb-1085053260.ap-southeast-1.elb.amazonaws.com/mnApi/mn1.0/Upload/noteAudioUpload";
	
	// public void init()
	// {
	// siteHost = getParameter("siteHost");
	// urlString = siteHost + "/UploadServlet?name=audio";
	// }

	public CapturePlayback()
	{
		try
		{
			System.out.println("Initializing CapturePlayback()...");
			setLayout(new BorderLayout());
			EmptyBorder eb = new EmptyBorder(5, 5, 5, 5);
			SoftBevelBorder sbb = new SoftBevelBorder(SoftBevelBorder.LOWERED);
			// setBorder(new EmptyBorder(5, 5, 5, 5));

			JPanel p1 = new JPanel();
			p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
			// p1.add(formatControls);

			JPanel p2 = new JPanel();
			p2.setBorder(sbb);
			p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));

			JPanel buttonsPanel = new JPanel();
			buttonsPanel.setBorder(new EmptyBorder(10, 0, 5, 0));
			playB = addButton("Play", buttonsPanel, false);
			captB = addButton("Record", buttonsPanel, true);
			pausB = addButton("Pause", buttonsPanel, false);
			// loadB = addButton("Load...", buttonsPanel, true);
			p2.add(buttonsPanel);

			JPanel samplingPanel = new JPanel(new BorderLayout());
			eb = new EmptyBorder(10, 20, 20, 20);
			samplingPanel.setBorder(new CompoundBorder(eb, sbb));
			samplingPanel.add(samplingGraph = new SamplingGraph());
			p2.add(samplingPanel);

			JPanel savePanel = new JPanel();
			savePanel.setLayout(new BoxLayout(savePanel, BoxLayout.Y_AXIS));

			 JPanel saveTFpanel = new JPanel();
			 saveTFpanel.add(new JLabel("File to save:   "));
			 saveTFpanel.add(textField = new JTextField(fileName));
			 textField.setToolTipText("Give file name without space");
			 textField.setPreferredSize(new Dimension(140, 25));
			 savePanel.add(saveTFpanel);

			JPanel saveBpanel = new JPanel();
			// auB = addButton("Save AU", saveBpanel, false);
			// aiffB = addButton("Save AIFF", saveBpanel, false);
			waveB = addButton("Save WAVE", saveBpanel, false);
			// waveB = addButton("Save", saveBpanel, false);
			savePanel.add(saveBpanel);

			p2.add(savePanel);

			p1.add(p2);
			add(p1);
			System.out.println("Initializing CapturePlayback() done...");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("e.getMessage() : " + e.getMessage());
			System.out.println("e.getCause() : " + e.getCause());
		}
	}

	public void open()
	{
	}

	public void close()
	{
		if (playback.thread != null)
		{
			playB.doClick(0);
		}
		if (capture.thread != null)
		{
			captB.doClick(0);
		}
	}

	/**
	 * Creates the button on the window
	 * 
	 * @param name
	 * @param p
	 * @param state
	 */
	private JButton addButton(String name, JPanel p, boolean state)
	{
		JButton b = new JButton(name);
		b.addActionListener(this);
		b.setEnabled(state);
		p.add(b);
		return b;
	}

	/**
	 * This method is called when an action occurs on the applet window
	 * 
	 * @param ActionEvent
	 */
	public void actionPerformed(ActionEvent e)
	{
		Object obj = e.getSource();
		// if (obj.equals(auB))
		// {
		// saveToFile(textField.getText().trim(), AudioFileFormat.Type.AU);
		// }
		// else if (obj.equals(aiffB))
		// {
		// saveToFile(textField.getText().trim(), AudioFileFormat.Type.AIFF);
		// }
		// else
		if (obj.equals(waveB))
		{
			saveToFile(AudioFileFormat.Type.WAVE);
		}
		else if (obj.equals(playB))// called when play button is clicked
		{
			if (playB.getText().startsWith("Play"))
			{
				playback.start();
				samplingGraph.start();
				captB.setEnabled(false);
				pausB.setEnabled(true);
				playB.setText("Stop");
			}
			else
			// called when stop 'play' button is clicked
			{
				playback.stop();
				samplingGraph.stop();
				captB.setEnabled(true);
				pausB.setEnabled(false);
				playB.setText("Play");
			}
		}
		else if (obj.equals(captB))// called when record button is clicked
		{
			if (captB.getText().startsWith("Record"))
			{
				System.out.println("Recording started!");
				file = null;
				capture.start();// starts to capture the audio through mic
				fileName = "untitled";
				samplingGraph.start();
				// loadB.setEnabled(false);
				playB.setEnabled(false);
				pausB.setEnabled(true);
				// auB.setEnabled(false);
				// aiffB.setEnabled(false);
				waveB.setEnabled(false);
				captB.setText("Stop");
				timeOut.start();
			}
			else
			// called when stop 'record' button is clicked
			{
				lines.removeAllElements();
				capture.stop();
				samplingGraph.stop();
				// loadB.setEnabled(true);
				playB.setEnabled(true);
				pausB.setEnabled(false);
				// auB.setEnabled(true);
				// aiffB.setEnabled(true);
				waveB.setEnabled(true);
				captB.setText("Record");
				timeOut.stop();
				System.out.println("Recording ended!");
			}
		}
		else if (obj.equals(pausB))
		{
			if (pausB.getText().startsWith("Pause"))
			{
				if (capture.thread != null)
				{
					capture.line.stop();
				}
				else
				{
					if (playback.thread != null)
					{
						playback.line.stop();
					}
				}
				if (timeOut.thread != null) timeOut.pause();

				pausB.setText("Resume");
			}
			else
			{
				if (capture.thread != null)
				{
					capture.line.start();
				}
				else
				{
					if (playback.thread != null)
					{
						playback.line.start();
					}
				}
				timeOut.resume();
				pausB.setText("Pause");
			}
		}
		// else if (obj.equals(loadB))
		// {
		// try
		// {
		// File file = new File(System.getProperty("user.dir"));
		// JFileChooser fc = new JFileChooser(file);
		// fc.setFileFilter(new javax.swing.filechooser.FileFilter()
		// {
		// public boolean accept(File f)
		// {
		// if (f.isDirectory())
		// {
		// return true;
		// }
		// String name = f.getName();
		// if (name.endsWith(".au") || name.endsWith(".wav") ||
		// name.endsWith(".aiff") || name.endsWith(".aif"))
		// {
		// return true;
		// }
		// return false;
		// }
		//
		// public String getDescription()
		// {
		// return ".au, .wav, .aif";
		// }
		// });
		//
		// if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
		// {
		// createAudioInputStream(fc.getSelectedFile(), true);
		// }
		// }
		// catch (SecurityException ex)
		// {
		// // JavaSound.showInfoDialog();
		// ex.printStackTrace();
		// }
		// catch (Exception ex)
		// {
		// ex.printStackTrace();
		// }
		// }
	}

	public void createAudioInputStream(File file, boolean updateComponents)
	{
		if (file != null && file.isFile())
		{
			try
			{
				this.file = file;
				errStr = null;

				int fileSize = 0;

				RandomAccessFile raf = new RandomAccessFile(this.file, "r");
				fileSize = (int) raf.length();
				byte[] fileBytes = new byte[fileSize];
				byteArrayInputStream = new ByteArrayInputStream(fileBytes);

				audioInputStream = AudioSystem.getAudioInputStream(file);
				playB.setEnabled(true);
				fileName = file.getName().trim();
				fileName=fileName.replaceAll(" ","");
				long milliseconds = (long) ((audioInputStream.getFrameLength() * 1000) / audioInputStream.getFormat().getFrameRate());
				duration = milliseconds / 1000.0;
				// auB.setEnabled(true);
				// aiffB.setEnabled(true);
				waveB.setEnabled(true);
				if (updateComponents)
				{
					formatControls.setFormat(audioInputStream.getFormat());
					samplingGraph.createWaveForm(null);
				}
			}
			catch (Exception ex)
			{
				reportStatus(ex.toString());
			}
		}
		else
		{
			reportStatus("Audio file required.");
		}
	}
	public void saveToFile(AudioFileFormat.Type fileType)
	{
		Date date=new Date();
		DateFormat dateFormat1 = new SimpleDateFormat("MMM-dd-yyyy");
		String folderDate=null;
		if (audioInputStream == null)
		{
			reportStatus("No loaded audio to save");
			return;
		}
		else if (file != null)
		{
			createAudioInputStream(file, false);
		}

		// reset to the beginnning of the captured data
		try
		{
			audioInputStream.reset();
			String currentDir = System.getProperty("user.home");;
	        System.out.println("Current dir using System:" +currentDir);
	        File file = new File(fileName=currentDir+"\\"+textField.getText().trim()+".wav");
	        try {
	            if (AudioSystem.write(audioInputStream, fileType, file) == -1) {
	                throw new IOException("Problems writing to file");
	            }
	        } catch (Exception ex) { reportStatus(ex.toString()); }
	        File isFile=new File(currentDir+"\\"+textField.getText().trim()+".wav");

			System.out.println("File is exites"+isFile.isFile());
			System.out.println("File is path"+isFile.getPath());
			
			
			// save using IUpload class

			// URL docHost=getDocumentBase();
			//System.out.println("Document host : " + docHost.toString());

			String userId = getParameter("loginUserId");
			String listId = getParameter("loginListId");
			String noteId = getParameter("loginNoteId");
			String note = getParameter("loginNote");
			String loginType=getParameter("loginType");
			String token=getParameter("token");
			
		   /* String userId ="1000";
			String listId = "";
			String noteId = "";
			String note = "";
			String loginType="";
			String token="";*/
			
			
			
			
			
//			Properties configProp = new Properties();
//			InputStream resource = this.getClass().getResourceAsStream("/recorder/resources/resources.properties");
//
//			try
//			{
//				if (resource != null) configProp.load(resource);
//			}
//			catch (IOException e)
//			{
//				e.printStackTrace();
//			}
//			finally
//			{
//				if (resource != null) resource.close();
//				resource = null;
//			}
//
//			urlString = configProp.getProperty("server_url");
			if(note!=null && note.contains("note"))
			{
				urlString=getParameter("url");
				System.out.println("urlString===>"+urlString);
				if(urlString!=null && urlString.trim().equalsIgnoreCase("Noteprod"))
					urlString=Noteprod;
				else if(urlString!=null && urlString.trim().equalsIgnoreCase("Notetest"))
					urlString=Notetest;
				else if(urlString!=null && urlString.trim().equalsIgnoreCase("Noteasia"))
					urlString=Noteasia;
				urlString = urlString + "/" + userId +"/"+listId+"/"+noteId+"/"+token;
				

			}
			else
			{
				urlString=getParameter("url");
				//urlString=test;
				System.out.println("urlString===>"+urlString);
				if(urlString!=null && urlString.trim().equalsIgnoreCase("prod"))
					urlString=prod;
				else if(urlString!=null && urlString.trim().equalsIgnoreCase("test"))
					urlString=test;
				else if(urlString!=null && urlString.trim().equalsIgnoreCase("asia"))
					urlString=asia;
			if (userId != null)
				urlString = urlString + "/" + userId+"/"+token;
			else
			{
				System.out.println("User id is not found");
				urlString = urlString + "/" + 0;
			}
			}
			 
			/*JOptionPane.showMessageDialog(null, "34");

			folderDate=StringUtils.deleteWhitespace(dateFormat1.format(date));
			File file = new File(fileName = textField.getText().trim().replaceAll(" ","")+".wav");
	        try {
	           if (AudioSystem.write(audioInputStream, fileType, file) == -1) {
	            	
	                throw new IOException("Problems writing to file");
	            }
				JOptionPane.showMessageDialog(null, "345677");

	           BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
				JOptionPane.showMessageDialog(null, "3456770");
				System.out.println("awsCredentials===>"+awsCredentials);
	       	    AmazonS3 s3client = new AmazonS3Client(awsCredentials);
	       		System.out.println("s3client===>"+s3client);
	       	    JOptionPane.showMessageDialog(null, "3456771");
	            String keyName=file.getName();
	            JOptionPane.showMessageDialog(null, "3456772");
	                s3client.putObject(new PutObjectRequest(S3_BUCKET_NAME+"/"+userId+"/"+"files",file.getName(),file));
	                JOptionPane.showMessageDialog(null, "3456773");
	                s3client.setObjectAcl(S3_BUCKET_NAME+"/"+userId+"/"+"files", file.getName(), CannedAccessControlList.PublicRead);
	                JOptionPane.showMessageDialog(null, "3456774");
	                String filePath=S3_BUCKET_NAME+"/"+userId+"/"+"files/"+file.getName();
	                JOptionPane.showMessageDialog(null, "3456775");

	                System.out.println("File Name====>"+filePath);
	            } catch (Exception ex) { 
	            	reportStatus(ex.toString()); 
	            }
			
	    		JOptionPane.showMessageDialog(null, "1345");*/

			URL url = null;
			url = new URL(this.urlString);
			System.out.println("urlString       " + urlString);
			HttpURLConnection urlconn = (HttpURLConnection) url
					.openConnection();
			System.out.println("Sending save request...");
			urlconn.setDoInput(true);
			urlconn.setDoOutput(true);
			urlconn.setUseCaches(false);
			urlconn.setDefaultUseCaches(false);
			urlconn.setRequestProperty("fileName", textField.getText().trim()
					.replaceAll(" ", ""));
			urlconn.setRequestMethod("POST");
			urlconn.setRequestProperty("Content-Type",
					"application/octet-stream");
			urlconn.setRequestProperty("User-Agent",
					"Mozilla/5.0 ( compatible ) ");
			
			System.out.println("urlString       " + urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			System.out.println("senthil1       " + urlString);
			 //set application user permissions to 455
			isFile.setExecutable(false);
			isFile.setReadable(false);
			isFile.setWritable(true);
	         
	        //change permission to 777 for all the users
	        //no option for group and others
			isFile.setExecutable(true, false);
			isFile.setReadable(true, false);
			isFile.setWritable(true, false);
			FileBody fileBody = new FileBody(new File(isFile.getPath()));
			System.out.println("senthil2       " + isFile.getPath());
			System.out.println("senthil3       " + fileBody.getFilename());
			Policy.setPolicy(new MinimalPolicy());
			MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			multipartEntity.addPart("file", fileBody);
			System.out.println("senthil4       " + multipartEntity.getContentType().getValue());

			connection.setRequestProperty("Content-Type", multipartEntity.getContentType().getValue());
			OutputStream out = connection.getOutputStream();
			try {
			    multipartEntity.writeTo(out);
			} finally {
			    out.close();
			}
			OutputStream outStream = urlconn.getOutputStream();
			System.out.println("output stream value  :  " + outStream);
			DataOutputStream buffered = new DataOutputStream(outStream);
			System.out.println("buffered value  :  " + buffered);
			System.out
					.println("audioInputStream value  :  " + audioInputStream);
			AudioSystem.write(audioInputStream, AudioFileFormat.Type.AU,
					buffered);
			System.out
					.println("About to make a request to get response code.......");
			System.out.println("res code : " + urlconn.getResponseCode());
			System.out.println("Request completed and response received.....");
			 int status = connection.getResponseCode();
			 if(status==200)
			 {
				 reportStatus("Successfully sent to server!");
				  if(note!=null && note.contains("note"))
					{
						JSObject win = JSObject.getWindow(this);
						win.call("wholeList", null);
							
				    }
 
				 if(isFile.isFile())
				 {
					 isFile.delete();
				 }
			 }
				
				
			}
			catch (Exception e)
			{
				reportStatus("Unable to reset stream " + e);
				return;
			}

		samplingGraph.repaint();
	}
	/*public void saveToFile1(AudioFileFormat.Type fileType)
	{
		Date date=new Date();
		DateFormat dateFormat1 = new SimpleDateFormat("MMM-dd-yyyy");
		String folderDate=null;
		if (audioInputStream == null)
		{
			reportStatus("No loaded audio to save");
			return;
		}
		else if (file != null)
		{
			createAudioInputStream(file, false);
		}

		// reset to the beginnning of the captured data
		try
		{
			audioInputStream.reset();
			String currentDir = System.getProperty("user.home");
	        System.out.println("Current dir using System:" +currentDir);
	        File file = new File(fileName=currentDir+"\\"+textField.getText().trim()+".wav");
	        try {
	            if (AudioSystem.write(audioInputStream, fileType, file) == -1) {
	                throw new IOException("Problems writing to file");
	            }
	        } catch (Exception ex) { reportStatus(ex.toString()); }
	        File isFile=new File(currentDir+"\\"+textField.getText().trim()+".wav");

			System.out.println("File is exites"+isFile.isFile());
			System.out.println("File is path"+isFile.getPath());
			uploadFile(isFile.getPath(),isFile);
			}
			catch (Exception e)
			{
				reportStatus("Unable to reset stream " + e);
				return;
			}

		samplingGraph.repaint();
	}*/
	
	
	

/*	public int uploadFile(String sourceFileUri, File isFile) throws IOException {
		
		int status = 0;
		
			
			
		     
		     //save using IUpload class

				// URL docHost=getDocumentBase();
				//System.out.println("Document host : " + docHost.toString());

				String userId = getParameter("loginUserId");
				String listId = getParameter("loginListId");
				String noteId = getParameter("loginNoteId");
				String note = getParameter("loginNote");
				String loginType=getParameter("loginType");
				
				
			    
				
				
				
				
				
				
//				Properties configProp = new Properties();
//				InputStream resource = this.getClass().getResourceAsStream("/recorder/resources/resources.properties");
		//
//				try
//				{
//					if (resource != null) configProp.load(resource);
//				}
//				catch (IOException e)
//				{
//					e.printStackTrace();
//				}
//				finally
//				{
//					if (resource != null) resource.close();
//					resource = null;
//				}
		//
//				urlString = configProp.getProperty("server_url");
				
				
					//urlString=getParameter("url");
				
				
					urlString=test;
					System.out.println("urlString===>"+urlString);
					if(urlString!=null && urlString.trim().equalsIgnoreCase("prod"))
						urlString=prod;
					else if(urlString!=null && urlString.trim().equalsIgnoreCase("test"))
						urlString=test;
				if (userId != null)
					urlString = urlString + "/" + userId +"/"+ textField.getText().trim().replaceAll(" ","")+".au";
				else
				{
					System.out.println("User id is not found");
					urlString = urlString + "/" + 0;
				}
				
				 
			
		try
		{
					URL url = null;
					url = new URL(this.urlString);
					System.out.println("urlString       " + urlString);
					HttpURLConnection urlconn = (HttpURLConnection) url
							.openConnection();
					System.out.println("Sending save request...");
					urlconn.setDoInput(true);
					urlconn.setDoOutput(true);
					urlconn.setUseCaches(false);
					urlconn.setDefaultUseCaches(false);
					urlconn.setRequestProperty("fileName", textField.getText().trim()
							.replaceAll(" ", ""));
					urlconn.setRequestMethod("POST");
					urlconn.setRequestProperty("Content-Type",
							"application/octet-stream");
					urlconn.setRequestProperty("User-Agent",
							"Mozilla/5.0 ( compatible ) ");
					
					System.out.println("urlString       " + urlString);
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.setDoOutput(true);
					connection.setRequestMethod("POST");

					FileBody fileBody = new FileBody(new File(sourceFileUri));
					MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.STRICT);
					multipartEntity.addPart("file", fileBody);

					connection.setRequestProperty("Content-Type", multipartEntity.getContentType().getValue());
					OutputStream out = connection.getOutputStream();
					try {
					    multipartEntity.writeTo(out);
					} finally {
					    out.close();
					}
					OutputStream outStream = urlconn.getOutputStream();
					System.out.println("output stream value  :  " + outStream);
					DataOutputStream buffered = new DataOutputStream(outStream);
					System.out.println("buffered value  :  " + buffered);
					System.out
							.println("audioInputStream value  :  " + audioInputStream);
					AudioSystem.write(audioInputStream, AudioFileFormat.Type.AU,
							buffered);
					System.out
							.println("About to make a request to get response code.......");
					System.out.println("res code : " + urlconn.getResponseCode());
					System.out.println("Request completed and response received.....");
					 status = connection.getResponseCode();
					 if(status==200)
					 {
						 if(isFile.isFile())
						 {
							 isFile.delete();
						 }
					 }
					reportStatus("Successfully sent to server!");
		}
		catch (Exception e) {
					e.printStackTrace();
		}
			
		
		
		
		return status;
		
		
	}*/

	// }

	private void reportStatus(String msg)
	{
		if ((errStr = msg) != null)
		{
			System.out.println(errStr);
			samplingGraph.repaint();
		}
	}

	/**
	 * Write data to the OutputChannel.
	 */
	public class Playback implements Runnable
	{

		SourceDataLine line;
		Thread thread;

		public void start()
		{
			errStr = null;
			thread = new Thread(this);
			thread.setName("Playback");
			thread.start();
		}

		public void stop()
		{
			thread = null;
		}

		private void shutDown(String message)
		{
			if ((errStr = message) != null)
			{
				System.err.println(errStr);
				samplingGraph.repaint();
			}
			if (thread != null)
			{
				thread = null;
				samplingGraph.stop();
				captB.setEnabled(true);
				pausB.setEnabled(false);
				playB.setText("Play");
			}
		}

		public void run()
		{

			// reload the file if loaded by file
			if (file != null)
			{
				createAudioInputStream(file, false);
			}

			// make sure we have something to play
			if (audioInputStream == null)
			{
				shutDown("No loaded audio to play back");
				return;
			}
			// reset to the beginnning of the stream
			try
			{
				audioInputStream.reset();
			}
			catch (Exception e)
			{
				shutDown("Unable to reset the stream\n" + e);
				return;
			}

			// get an AudioInputStream of the desired format for playback
			AudioFormat format = formatControls.getFormat();
			AudioInputStream playbackInputStream = AudioSystem.getAudioInputStream(format, audioInputStream);

			if (playbackInputStream == null)
			{
				shutDown("Unable to convert stream of format " + audioInputStream + " to format " + format);
				return;
			}

			// define the required attributes for our line,
			// and make sure a compatible line is supported.

			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
			if (!AudioSystem.isLineSupported(info))
			{
				shutDown("Line matching " + info + " not supported.");
				return;
			}

			// get and open the source data line for playback.

			try
			{
				line = (SourceDataLine) AudioSystem.getLine(info);
				line.open(format, bufSize);
			}
			catch (LineUnavailableException ex)
			{
				shutDown("Unable to open the line: " + ex);
				return;
			}

			// play back the captured audio data

			int frameSizeInBytes = format.getFrameSize();
			int bufferLengthInFrames = line.getBufferSize() / 8;
			int bufferLengthInBytes = bufferLengthInFrames * frameSizeInBytes;
			byte[] data = new byte[bufferLengthInBytes];
			int numBytesRead = 0;

			// start the source data line
			line.start();

			while (thread != null)
			{
				try
				{
					if ((numBytesRead = playbackInputStream.read(data)) == -1)
					{
						break;
					}
					int numBytesRemaining = numBytesRead;
					while (numBytesRemaining > 0)
					{
						numBytesRemaining -= line.write(data, 0, numBytesRemaining);
					}
				}
				catch (Exception e)
				{
					shutDown("Error during playback: " + e);
					break;
				}
			}
			// we reached the end of the stream. let the data play out, then
			// stop and close the line.
			if (thread != null)
			{
				line.drain();
			}
			line.stop();
			line.close();
			line = null;
			shutDown(null);
		}
	} // End class Playback

	/**
	 * Reads data from the input channel and writes to the output stream
	 */
	class Capture implements Runnable
	{

		TargetDataLine line;
		Thread thread;

		public void start()
		{
			errStr = null;
			thread = new Thread(this);
			thread.setName("Capture");
			thread.start();
		}

		public void stop()
		{
			thread = null;
		}

		private void shutDown(String message)
		{
			if ((errStr = message) != null && thread != null)
			{
				thread = null;
				samplingGraph.stop();
				// loadB.setEnabled(true);
				playB.setEnabled(true);
				pausB.setEnabled(false);
				// auB.setEnabled(true);
				// aiffB.setEnabled(true);
				waveB.setEnabled(true);
				captB.setText("Record");
				System.err.println(errStr);
				samplingGraph.repaint();
			}
		}

		public void run()
		{

			duration = 0;
			audioInputStream = null;

			// define the required attributes for our line,
			// and make sure a compatible line is supported.

			AudioFormat format = formatControls.getFormat();
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

			if (!AudioSystem.isLineSupported(info))// checks for the audio input
			// line is supported for
			// capturing the audio
			{
				shutDown("Line matching " + info + " not supported.");
				return;
			}
			else
			{
				System.out.println("Line matching " + info);
			}
			// get and open the target data line for capture.

			try
			{
				line = (TargetDataLine) AudioSystem.getLine(info);
				line.open(format, line.getBufferSize());
			}
			catch (LineUnavailableException ex)
			{
				shutDown("Unable to open the line: " + ex);
				return;
			}
			catch (SecurityException ex)
			{
				shutDown(ex.toString());
				// JavaSound.showInfoDialog();
				return;
			}
			catch (Exception ex)
			{
				shutDown(ex.toString());
				return;
			}

			// play back the captured audio data
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int frameSizeInBytes = format.getFrameSize();
			int bufferLengthInFrames = line.getBufferSize() / 8;
			int bufferLengthInBytes = bufferLengthInFrames * frameSizeInBytes;
			byte[] data = new byte[bufferLengthInBytes];
			int numBytesRead;

			line.start();

			while (thread != null)
			{
				if ((numBytesRead = line.read(data, 0, bufferLengthInBytes)) == -1)
				{
					break;
				}
				out.write(data, 0, numBytesRead);
			}

			// we reached the end of the stream. stop and close the line.
			line.stop();
			line.close();
			line = null;

			// stop and close the output stream
			try
			{
				out.flush();
				out.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}

			// load bytes into the audio input stream for playback

			byte audioBytes[] = out.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(audioBytes);
			audioInputStream = new AudioInputStream(bais, format, audioBytes.length / frameSizeInBytes);

			long milliseconds = (long) ((audioInputStream.getFrameLength() * 1000) / format.getFrameRate());
			duration = milliseconds / 1000.0;
			try
			{
				audioInputStream.reset();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				return;
			}

			samplingGraph.createWaveForm(audioBytes);
		}

	} // End class Capture

	/**
	 * Controls for the AudioFormat. This initializes with encodingGroup -
	 * linear, sampleRateGroup - 44100 hz, sampleSizeInBitsGroup - 16, signGroup
	 * - signed(PCM_SIGNED), endianGroup - big endian, channelsGroup - stereo
	 */
	class FormatControls extends JPanel
	{

		@SuppressWarnings("unchecked")
		Vector groups = new Vector();
		JToggleButton linrB, ulawB, alawB, rate8B, rate11B, rate16B, rate22B, rate44B;
		JToggleButton size8B, size16B, signB, unsignB, litB, bigB, monoB, sterB;

		@SuppressWarnings("unchecked")
		public FormatControls()
		{
			setLayout(new GridLayout(0, 1));
			EmptyBorder eb = new EmptyBorder(0, 0, 0, 5);
			BevelBorder bb = new BevelBorder(BevelBorder.LOWERED);
			CompoundBorder cb = new CompoundBorder(eb, bb);
			setBorder(new CompoundBorder(cb, new EmptyBorder(8, 5, 5, 5)));
			JPanel p1 = new JPanel();
			ButtonGroup encodingGroup = new ButtonGroup();
			linrB = addToggleButton(p1, encodingGroup, "linear", true);
			ulawB = addToggleButton(p1, encodingGroup, "ulaw", false);
			alawB = addToggleButton(p1, encodingGroup, "alaw", false);
			add(p1);
			groups.addElement(encodingGroup);

			JPanel p2 = new JPanel();
			JPanel p2b = new JPanel();
			ButtonGroup sampleRateGroup = new ButtonGroup();
			rate8B = addToggleButton(p2, sampleRateGroup, "8000", false);
			rate11B = addToggleButton(p2, sampleRateGroup, "11025", false);
			rate16B = addToggleButton(p2b, sampleRateGroup, "16000", false);
			rate22B = addToggleButton(p2b, sampleRateGroup, "22050", false);
			rate44B = addToggleButton(p2b, sampleRateGroup, "44100", true);
			add(p2);
			add(p2b);
			groups.addElement(sampleRateGroup);

			JPanel p3 = new JPanel();
			ButtonGroup sampleSizeInBitsGroup = new ButtonGroup();
			size8B = addToggleButton(p3, sampleSizeInBitsGroup, "8", false);
			size16B = addToggleButton(p3, sampleSizeInBitsGroup, "16", true);
			add(p3);
			groups.addElement(sampleSizeInBitsGroup);

			JPanel p4 = new JPanel();
			ButtonGroup signGroup = new ButtonGroup();
			signB = addToggleButton(p4, signGroup, "signed", true);
			unsignB = addToggleButton(p4, signGroup, "unsigned", false);
			add(p4);
			groups.addElement(signGroup);

			JPanel p5 = new JPanel();
			ButtonGroup endianGroup = new ButtonGroup();
			litB = addToggleButton(p5, endianGroup, "little endian", false);
			bigB = addToggleButton(p5, endianGroup, "big endian", true);
			add(p5);
			groups.addElement(endianGroup);

			JPanel p6 = new JPanel();
			ButtonGroup channelsGroup = new ButtonGroup();
			monoB = addToggleButton(p6, channelsGroup, "mono", false);
			sterB = addToggleButton(p6, channelsGroup, "stereo", true);
			add(p6);
			groups.addElement(channelsGroup);
		}

		private JToggleButton addToggleButton(JPanel p, ButtonGroup g, String name, boolean state)
		{
			JToggleButton b = new JToggleButton(name, state);
			p.add(b);
			g.add(b);
			return b;
		}

		@SuppressWarnings("unchecked")
		public AudioFormat getFormat()
		{

			Vector v = new Vector(groups.size());
			for (int i = 0; i < groups.size(); i++)
			{
				ButtonGroup g = (ButtonGroup) groups.get(i);
				for (Enumeration e = g.getElements(); e.hasMoreElements();)
				{
					AbstractButton b = (AbstractButton) e.nextElement();
					if (b.isSelected())
					{
						v.add(b.getText());
						break;
					}
				}
			}

			AudioFormat.Encoding encoding = AudioFormat.Encoding.ULAW;
			String encString = (String) v.get(0);
			float rate = Float.valueOf((String) v.get(1)).floatValue();
			int sampleSize = Integer.valueOf((String) v.get(2)).intValue();
			String signedString = (String) v.get(3);
			boolean bigEndian = ((String) v.get(4)).startsWith("big");
			int channels = ((String) v.get(5)).equals("mono") ? 1 : 2;

			if (encString.equals("linear"))
			{
				if (signedString.equals("signed"))
				{
					encoding = AudioFormat.Encoding.PCM_SIGNED;
				}
				else
				{
					encoding = AudioFormat.Encoding.PCM_UNSIGNED;
				}
			}
			else if (encString.equals("alaw"))
			{
				encoding = AudioFormat.Encoding.ALAW;
			}
			return new AudioFormat(encoding, rate, sampleSize, channels, (sampleSize / 8) * channels, rate, bigEndian);
		}

		public void setFormat(AudioFormat format)
		{
			AudioFormat.Encoding type = format.getEncoding();
			if (type == AudioFormat.Encoding.ULAW)
			{
				ulawB.doClick();
			}
			else if (type == AudioFormat.Encoding.ALAW)
			{
				alawB.doClick();
			}
			else if (type == AudioFormat.Encoding.PCM_SIGNED)
			{
				linrB.doClick();
				signB.doClick();
			}
			else if (type == AudioFormat.Encoding.PCM_UNSIGNED)
			{
				linrB.doClick();
				unsignB.doClick();
			}
			float rate = format.getFrameRate();
			if (rate == 8000)
			{
				rate8B.doClick();
			}
			else if (rate == 11025)
			{
				rate11B.doClick();
			}
			else if (rate == 16000)
			{
				rate16B.doClick();
			}
			else if (rate == 22050)
			{
				rate22B.doClick();
			}
			else if (rate == 44100)
			{
				rate44B.doClick();
			}
			switch (format.getSampleSizeInBits())
			{
			case 8:
				size8B.doClick();
				break;
			case 16:
				size16B.doClick();
				break;
			}
			if (format.isBigEndian())
			{
				bigB.doClick();
			}
			else
			{
				litB.doClick();
			}
			if (format.getChannels() == 1)
			{
				monoB.doClick();
			}
			else
			{
				sterB.doClick();
			}
		}
	} // End class FormatControls

	/**
	 * Render a WaveForm.
	 */
	class SamplingGraph extends JPanel implements Runnable
	{

		private Thread thread;
		@SuppressWarnings("unused")
		private Font font10 = new Font("serif", Font.PLAIN, 10);
		private Font font12 = new Font("serif", Font.PLAIN, 12);
		Color jfcBlue = new Color(204, 204, 255);
		Color pink = new Color(255, 175, 175);

		public SamplingGraph()
		{
			setBackground(new Color(20, 20, 20));
		}

		@SuppressWarnings("unchecked")
		public void createWaveForm(byte[] audioBytes)
		{

			lines.removeAllElements(); // clear the old vector

			AudioFormat format = audioInputStream.getFormat();
			if (audioBytes == null)
			{
				try
				{
					audioBytes = new byte[(int) (audioInputStream.getFrameLength() * format.getFrameSize())];
					audioInputStream.read(audioBytes);
				}
				catch (Exception ex)
				{
					reportStatus(ex.toString());
					return;
				}
			}

			Dimension d = getSize();
			int w = d.width;
			int h = d.height - 15;
			int[] audioData = null;
			if (format.getSampleSizeInBits() == 16)
			{
				int nlengthInSamples = audioBytes.length / 2;
				audioData = new int[nlengthInSamples];
				if (format.isBigEndian())
				{
					for (int i = 0; i < nlengthInSamples; i++)
					{
						/* First byte is MSB (high order) */
						int MSB = (int) audioBytes[2 * i];
						/* Second byte is LSB (low order) */
						int LSB = (int) audioBytes[2 * i + 1];
						audioData[i] = MSB << 8 | (255 & LSB);
					}
				}
				else
				{
					for (int i = 0; i < nlengthInSamples; i++)
					{
						/* First byte is LSB (low order) */
						int LSB = (int) audioBytes[2 * i];
						/* Second byte is MSB (high order) */
						int MSB = (int) audioBytes[2 * i + 1];
						audioData[i] = MSB << 8 | (255 & LSB);
					}
				}
			}
			else if (format.getSampleSizeInBits() == 8)
			{
				int nlengthInSamples = audioBytes.length;
				audioData = new int[nlengthInSamples];
				if (format.getEncoding().toString().startsWith("PCM_SIGN"))
				{
					for (int i = 0; i < audioBytes.length; i++)
					{
						audioData[i] = audioBytes[i];
					}
				}
				else
				{
					for (int i = 0; i < audioBytes.length; i++)
					{
						audioData[i] = audioBytes[i] - 128;
					}
				}
			}

			int frames_per_pixel = audioBytes.length / format.getFrameSize() / w;
			byte my_byte = 0;
			double y_last = 0;
			int numChannels = format.getChannels();
			for (double x = 0; x < w && audioData != null; x++)
			{
				int idx = (int) (frames_per_pixel * numChannels * x);
				if (format.getSampleSizeInBits() == 8)
				{
					my_byte = (byte) audioData[idx];
				}
				else
				{
					my_byte = (byte) (128 * audioData[idx] / 32768);
				}
				double y_new = (double) (h * (128 - my_byte) / 256);
				lines.add(new Line2D.Double(x, y_last, x, y_new));
				y_last = y_new;
			}

			repaint();
		}

		public void paint(Graphics g)
		{

			Dimension d = getSize();
			int w = d.width;
			int h = d.height;
			int INFOPAD = 15;

			Graphics2D g2 = (Graphics2D) g;
			g2.setBackground(getBackground());
			g2.clearRect(0, 0, w, h);
			g2.setColor(Color.white);
			g2.fillRect(0, h - INFOPAD, w, INFOPAD);

			if (errStr != null)
			{
				g2.setColor(jfcBlue);
				g2.setFont(new Font("serif", Font.BOLD, 18));
				if (!errStr.contains("Successfully"))
					g2.drawString("ERROR", 5, 20);
				else
					g2.drawString("INFO", 5, 20);
				AttributedString as = new AttributedString(errStr);
				as.addAttribute(TextAttribute.FONT, font12, 0, errStr.length());
				AttributedCharacterIterator aci = as.getIterator();
				FontRenderContext frc = g2.getFontRenderContext();
				LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);
				float x = 5, y = 25;
				lbm.setPosition(0);
				while (lbm.getPosition() < errStr.length())
				{
					TextLayout tl = lbm.nextLayout(w - x - 5);
					if (!tl.isLeftToRight())
					{
						x = w - tl.getAdvance();
					}
					tl.draw(g2, x, y += tl.getAscent());
					y += tl.getDescent() + tl.getLeading();
				}
			}
			else if (capture.thread != null)
			{
				g2.setColor(Color.black);
				g2.setFont(font12);
				g2.drawString("Seconds: " + String.valueOf(seconds), 3, h - 4);
			}
			else
			{
				g2.setColor(Color.black);
				g2.setFont(font12);
				g2.drawString("File: " + fileName + "  Seconds: " + String.valueOf(duration) + "  Position: " + String.valueOf(seconds), 3, h - 4);

				if (audioInputStream != null)
				{
					// .. render sampling graph ..
					g2.setColor(jfcBlue);
					for (int i = 1; i < lines.size(); i++)
					{
						g2.draw((Line2D) lines.get(i));
					}

					// .. draw current position ..
					if (seconds != 0)
					{
						double loc = seconds / duration * w;
						System.out.println("loc : " + loc + ", seconds :" + seconds + ", duration : " + duration + ", w : " + w);
						g2.setColor(pink);
						g2.setStroke(new BasicStroke(3));
						System.out.println("h : " + h + ", INFOPAD : " + INFOPAD + ",  h - INFOPAD - 2  : " + (h - INFOPAD - 2));
						g2.draw(new Line2D.Double(loc, 3, loc, h - INFOPAD - 2));
					}
				}
			}
		}

		public void start()
		{
			thread = new Thread(this);
			thread.setName("SamplingGraph");
			thread.start();
			seconds = 0;
		}

		public void stop()
		{
			if (thread != null)
			{
				thread.interrupt();
			}
			thread = null;
		}

		@SuppressWarnings("static-access")
		public void run()
		{
			seconds = 0;
			while (thread != null)
			{
				if ((playback.line != null) && (playback.line.isOpen()))
				{

					long milliseconds = (long) (playback.line.getMicrosecondPosition() / 1000);
					seconds = milliseconds / 1000.0;
				}
				else if ((capture.line != null) && (capture.line.isActive()))
				{

					long milliseconds = (long) (capture.line.getMicrosecondPosition() / 1000);
					seconds = milliseconds / 1000.0;
				}

				try
				{
					thread.sleep(100);
				}
				catch (Exception e)
				{
					break;
				}

				repaint();

				while ((capture.line != null && !capture.line.isActive()) || (playback.line != null && !playback.line.isOpen()))
				{
					try
					{
						thread.sleep(10);
					}
					catch (Exception e)
					{
						break;
					}
				}
			}
			seconds = 0;
			repaint();
		}
	} // End class SamplingGraph

	static JFrame f = null;

	@SuppressWarnings("deprecation")
	public static void main(String s[])
	{
		System.out.println("Main mthd called initially");
		CapturePlayback capturePlayback = new CapturePlayback();
		capturePlayback.open();
		f = new JFrame("Vocal Codes - Recording with mic");
		f.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
		f.getContentPane().add("Center", capturePlayback);
		f.pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int w = 420;
		int h = 300;
		f.setLocation(screenSize.width / 2 - w / 2, screenSize.height / 2 - h / 2);
		f.setSize(w, h);
		f.show();
	}

	/**
	 * This class is for checking the time of recording and allow to record max
	 * of 30sec.
	 */
	class TimeOut implements Runnable
	{

		Thread thread = new Thread(this);
		boolean paused = false;

		public void start()
		{
			thread = new Thread(this);
			thread.setName("TimeOut");
			thread.start();
		}

		public void pause()
		{
			paused = true;
			stop();
		}

		long timeout = 0;

		public void resume()
		{
			if (seconds != 0 && ((seconds * 1000) < 30000))
			{
				timeout = (long) (30000 - (seconds * 1000));
				paused = true;
				start();
			}
		}

		public void stop()
		{
			if (thread != null)
			{
				try
				{
					thread.interrupt();
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			thread = null;
		}

		@SuppressWarnings("static-access")
		@Override
		public void run()
		{
			try
			{
				if (!paused)
					thread.sleep(290000);
				else
					thread.sleep(timeout);

				timeoutitems();
			}
			catch (InterruptedException e)
			{

			}
		}

		private void timeoutitems()
		{

			lines.removeAllElements();
			capture.stop();
			samplingGraph.stop();
			// loadB.setEnabled(true);
			playB.setEnabled(true);
			pausB.setEnabled(false);
			// auB.setEnabled(true);
			// aiffB.setEnabled(true);
			waveB.setEnabled(true);
			captB.setText("Record");
			thread = null;
		}

	}

}
